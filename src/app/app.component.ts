import { Component } from '@angular/core';
import { EChartOption, graphic } from 'echarts';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor() {
    this.genData();
  }

  base = +new Date(0, 0, 0);
  oneDay = 24 * 3600 * 1000;
  date = [];
  data = [];

  option: EChartOption = {
    tooltip: {
      trigger: 'axis',
      position: (pt) => {
        return [pt[0], '10%'];
      },
      axisPointer: {
        type: 'shadow',
        lineStyle: {
          type: 'dotted'
        }

      }
    },
    title: {
      left: 'left',
      text: 'Info',
    },
    xAxis: {
      type: 'category',
      boundaryGap: false,
      data: this.date,
      axisLine: {
        show: false,
      },
      axisTick: {
        show: false,
      },
    },
    yAxis: {
      type: 'value',
      boundaryGap: [0, '100%'],
      position: 'right',
      axisLine: {
        show: false,
      },
      axisTick: {
        show: false,
      },
      splitLine: {
        show: false,
      },
      max: 4.5,
      min: 1.5
    },

    series: [
      {
        name: 'Rate',
        type: 'line',
        smooth: false,
        symbol: '',
        sampling: 'average',
        itemStyle: {
          color: 'lightblue'
        },
        areaStyle: {
          color: new graphic.LinearGradient(0, 0, 0, 1, [{
            offset: 0,
            color: '#aaa'
          }, {
            offset: 1,
            color: '#eee'
          }])
        },
        data: this.data
      }
    ],
  };


  // genData = () => {
  //   for (let i = 0; i < 2000; i++) {

  //     const now = new Date(this.base += this.oneDay);
  //     this.date.push([now.getFullYear(), now.getMonth() + 1, now.getDate()].join('/'));
  //     this.data.push(Math.round((Math.random() - 0.5) * 20 + this.data[i - 1]));
  //   }
  // }

  genData = () => {
    this.date.push('JAN');
    this.date.push('FEB');
    this.date.push('MAR');
    this.date.push('APR');
    this.date.push('MAY');
    this.date.push('JUN');
    this.date.push('JUL');
    this.date.push('AUG');
    this.date.push('SEP');
    this.date.push('OCT');
    this.date.push('NOV');
    this.date.push('DEC');
    for (let i = 0; i < 12; i++) {
      this.data.push((Math.random() * 1 ) + 2.8);
    }

    console.log(this.data);
  }


}
